﻿using UnityEngine;

namespace JP.GameSparksTest.Misc
{
	[RequireComponent(typeof(RectTransform))]
	public class RectTransformRotator : MonoBehaviour
	{
		[SerializeField] private float rotationPerSecond;

		private RectTransform rectTransform;

		private void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
		}

		private void Update()
		{
			float rotation = rotationPerSecond * Time.deltaTime;
			rectTransform.localRotation = Quaternion.Euler(0f,0f, rectTransform.localEulerAngles.z + rotation);
		}
	}
}
