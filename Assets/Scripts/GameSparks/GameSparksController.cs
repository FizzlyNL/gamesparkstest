﻿using System;
using Facebook.Unity;
using GameSparks.Api.Responses;
using GameSparks.Platforms;
using UnityEngine;

namespace JP.GameSparksTest.Cloud
{
	public class GameSparksController : MonoBehaviour
	{
		public static GameSparksController Instance;
		
		public GameSparksSettings settings;

		public Action<AuthenticationResponse> OnAuthentication;
		public Action<RegistrationResponse> OnRegistration;
		public Action OnDisconnect;
		
		private void Start()
		{
#if UNITY_IOS && !UNITY_EDITOR
		this.gameObject.AddComponent<IOSPlatform>();
#elif UNITY_WEBGL && !UNITY_EDITOR
		this.gameObject.AddComponent<WebGLPlatform>();
#else
		this.gameObject.AddComponent<DefaultPlatform>();
#endif
		}
		
		private void Awake()
		{
			Instance = this;
		}

		public void AuthenticateWithFacebook(string accesToken)
		{
			new GameSparks.Api.Requests.FacebookConnectRequest()
				.SetAccessToken(accesToken)
				.Send(OnAuthenticationCallback);
		}
		
		public void Authenticate(string name, string password)
		{
			new GameSparks.Api.Requests.AuthenticationRequest()
				.SetUserName(name)
				.SetPassword(password)
				.Send(OnAuthenticationCallback);
		}

		private void OnAuthenticationCallback(AuthenticationResponse obj)
		{
			if(OnAuthentication!=null)
				OnAuthentication(obj);
		}

		public void Disconnect()
		{
			if(FB.IsLoggedIn)
				FB.LogOut();
			
			new GameSparks.Api.Requests.EndSessionRequest().Send(OnDisconnectCallback);
		}

		private void OnDisconnectCallback(EndSessionResponse obj)
		{
			if (OnDisconnect != null)
				OnDisconnect();
		}

		public void Register(string name, string password, string username)
		{
			new GameSparks.Api.Requests.RegistrationRequest()
				.SetDisplayName(name)
				.SetPassword(password)
				.SetUserName(username)
				.Send(OnRegisterCallback);
		}

		private void OnRegisterCallback(RegistrationResponse obj)
		{
			if (OnRegistration != null)
				OnRegistration(obj);
		}
	}
}
