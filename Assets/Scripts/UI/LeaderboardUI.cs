﻿using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
    public class LeaderboardUI : MonoBehaviour
    {
        [SerializeField] private Text positionText;
        [SerializeField] private Text playernameText;
        [SerializeField] private Text scoreText;
        
        public void Setup(int position, string playername, int score)
        {
            positionText.text = position.ToString();
            playernameText.text = playername;
            scoreText.text = score.ToString();
        }
    }
}