﻿using System;
using GameSparks.Api.Responses;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
    public class DebugStringPopup : BaseConnectedPopup
    {
        [SerializeField] private Button sendButton;
        [SerializeField] private Text responseText;
        [SerializeField] private InputField inputField;

        protected override void Show()
        {
            base.Show();
            sendButton.onClick.AddListener(OnClick);
        }

        protected override void Hide()
        {
            base.Hide();
            sendButton.onClick.RemoveAllListeners();

            responseText.text = "None";
        }

        private void OnClick()
        {
            SendTestData(OnTestDataResponse);
        }

        protected override void OnAuthentication(AuthenticationResponse obj)
        {
            base.OnAuthentication(obj);
            
            if (obj.HasErrors)
                responseText.text = "Init Error";
            else
                responseText.text = obj.ScriptData.GetString("SC_TestData");
        }

        private void OnTestDataResponse(CustomResponse obj)
        {
            if (obj.HasErrors)
                responseText.text = "Error";
            else
                responseText.text = obj.ScriptData.GetString("SC_TestData");
        }
        
        public void SendTestData(Action<CustomResponse> callback)
        {
            new GameSparks.Api.Requests.CustomRequest("LogEventRequest")
                .AddString("eventKey", "SC_TestEvent")
                .AddString("SC_TestData", inputField.text)
                .Send(callback);
        }
    }
}