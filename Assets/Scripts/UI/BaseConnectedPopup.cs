﻿using GameSparks.Api.Responses;
using JP.GameSparksTest.Cloud;
using UnityEngine;

namespace JP.GameSparksTest.UI
{
    public class BaseConnectedPopup : MonoBehaviour
    {
        private void Start()
        {
            GameSparksController.Instance.OnAuthentication += OnAuthentication;
            GameSparksController.Instance.OnDisconnect += OnDisconnect;
            Hide();
        }

        private void OnDisconnect()
        {
            Hide();
        }
        
        protected virtual void Show()
        {
            gameObject.SetActive(true);
        }

        protected virtual void Hide()
        {
            gameObject.SetActive(false);
        }
		
        protected virtual void OnAuthentication(AuthenticationResponse obj)
        {
            if (!obj.HasErrors)
            {
                Show();
            }
        }

    }
}