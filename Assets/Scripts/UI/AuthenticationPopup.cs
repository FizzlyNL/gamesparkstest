﻿using System.Collections.Generic;
using System.Reflection.Emit;
using Facebook.Unity;
using GameSparks.Api.Responses;
using JP.GameSparksTest.Cloud;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
	public class AuthenticationPopup : MonoBehaviour
	{
		public static AuthenticationPopup Instance;

		[SerializeField] private InputField usernameField;
		[SerializeField] private InputField passwordField;

		[SerializeField] private Button registerButton;
		[SerializeField] private Button loginButton;

		[SerializeField] private Button facebookLoginButton;

		private AccessToken accesToken;
		
		private void Awake()
		{
			Instance = this;
			
			if (!FB.IsInitialized) {
				// Initialize the Facebook SDK
				FB.Init(InitCallback, OnHideUnity);
			} else {
				// Already initialized, signal an app activation App Event
				FB.ActivateApp();
			}			
		}
		
		private void InitCallback ()
		{
			if (FB.IsInitialized) {
				// Signal an app activation App Event
				FB.ActivateApp();
				// Continue with Facebook SDK
				// ...
				

				
			} else {
				Debug.Log("Failed to Initialize the Facebook SDK");
			}
		}
		
		private void AuthCallback (ILoginResult result) {
			if (FB.IsLoggedIn) {
				// AccessToken class will have session details
				accesToken = Facebook.Unity.AccessToken.CurrentAccessToken;
				// Print current access token's User ID
				Debug.Log(accesToken.UserId);
				// Print current access token's granted permissions
				foreach (string perm in accesToken.Permissions)
					Debug.Log(perm);
				
				MessagePopup.Instance.ShowMessage("Facebook login succes!", "Proceed with gamesparks authorization", AuthorizeWithFacebook);
					
				
			} else {
				Debug.Log("User cancelled login");
			}
		}

		private void AuthorizeWithFacebook()
		{
			
			Debug.Log("Client token: " + FB.ClientToken);
			Debug.Log("Custom token: " + accesToken.TokenString);
			
			GameSparksController.Instance.AuthenticateWithFacebook(accesToken.TokenString);
			Hide();
		}
		
		private void OnHideUnity (bool isGameShown)
		{
			if (!isGameShown) {
				// Pause the game - we will need to hide
				Time.timeScale = 0;
			} else {
				// Resume the game - we're getting focus again
				Time.timeScale = 1;
			}
		}
		

		private void Start()
		{
			GameSparksController.Instance.OnAuthentication += OnAuthenticate;
			GameSparksController.Instance.OnRegistration += OnRegister;
			GameSparksController.Instance.OnDisconnect += OnDisconnect;

			Show();
		}

		private void OnDisconnect()
		{
			Show();
		}

		private void Show()
		{
			registerButton.onClick.AddListener(OnRegisterButton);
			loginButton.onClick.AddListener(OnLoginButton);
			facebookLoginButton.onClick.AddListener(OnFacebookLoginButton);
			
			gameObject.SetActive(true);
		}

		private void OnFacebookLoginButton()
		{
			var perms = new List<string>(){"public_profile", "email", "user_friends"};
			FB.LogInWithReadPermissions(perms, AuthCallback);
		}

		private void Hide()
		{
			gameObject.SetActive(false);
			
			registerButton.onClick.RemoveAllListeners();
			loginButton.onClick.RemoveAllListeners();
			facebookLoginButton.onClick.RemoveAllListeners();
		}

		private void OnLoginButton()
		{
			GameSparksController.Instance.Authenticate(usernameField.text, passwordField.text);
			Hide();
		}

		private void OnRegisterButton()
		{
			GameSparksController.Instance.Register(usernameField.text, passwordField.text, usernameField.text);
			Hide();
		}

		private void OnRegister(RegistrationResponse obj)
		{
			if (obj.HasErrors)
				MessagePopup.Instance.ShowMessage("Registration failed! :'(", obj.Errors.ToString(), Show);
			else
				MessagePopup.Instance.ShowMessage("Registration Succes! :D", obj.AuthToken, Show);
		}

		private void OnAuthenticate(AuthenticationResponse obj)
		{
			if (obj.HasErrors)
				MessagePopup.Instance.ShowMessage("Authentication failed! :'(", obj.Errors.ToString(), Show);
			else
				MessagePopup.Instance.ShowMessage("Authentication Succes! :D", obj.ScriptData.GetString("SC_Message"), Hide);
		}

		private void OnTestDataSent(CustomResponse obj)
		{
			Debug.Log("has errors: " + obj.HasErrors.ToString() );
			Debug.Log("Time is: " + obj.ScriptData.GetString("SC_TestData"));
		}
	}
}
