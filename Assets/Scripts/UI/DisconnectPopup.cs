﻿using JP.GameSparksTest.Cloud;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
	public class DisconnectPopup : BaseConnectedPopup
	{
		[SerializeField] private Button button;

		protected override void Show()
		{
			button.onClick.AddListener(OnClick);
			base.Show();
		}
		private void OnClick()
		{
			GameSparksController.Instance.Disconnect();
		}

		protected override  void Hide()
		{
			base.Hide();
			button.onClick.RemoveAllListeners();
		}

	}
}
