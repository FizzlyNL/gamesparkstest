﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
	public class MessagePopup : MonoBehaviour
	{
		[SerializeField] private Text titleText;
		[SerializeField] private Text messageText;

		[SerializeField] private Button button;
		
		public static MessagePopup Instance;

		private Action callback;
		
		private void Awake()
		{
			Instance = this;
			
			Hide();
		}

		public void ShowMessage(string title, string message, Action callback = null)
		{
			button.onClick.AddListener(OnButtonClick);

			titleText.text = title;
			messageText.text = message;

			this.callback = callback;
			
			Show();
		}

		private void OnButtonClick()
		{
			button.onClick.RemoveListener(OnButtonClick);
			
			Hide();
			
			if (callback != null)
				callback();
		}

		private void Show()
		{
			gameObject.SetActive(true);
		}

		private void Hide()
		{
			gameObject.SetActive(false);
		}
	}
}
