﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameSparks.Api.Responses;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
    public class LeaderboardPopup : BaseConnectedPopup
    {
        [SerializeField] private LeaderboardUI templateLeaderboardUi;

        [SerializeField] private Button requestDataButton;

        [SerializeField] private GameObject blocker;

        private List<LeaderboardUI> leaderboardUiList;

        private void Awake()
        {
            leaderboardUiList = new List<LeaderboardUI>();

            ResetLeaderboardUI();
        }

        protected override void Show()
        {
            base.Show();
            requestDataButton.onClick.AddListener(OnRequestDataButton);
        }

        protected override void Hide()
        {
            base.Hide();
            requestDataButton.onClick.RemoveAllListeners();
        }

        private void OnRequestDataButton()
        {
            requestDataButton.interactable = false;
            
            blocker.gameObject.SetActive(true);
            
            RequestData(OnDataRequestResponse);
        }

        private void ResetLeaderboardUI()
        {
            for (int i = leaderboardUiList.Count-1; i >= 0; i--)
            {
                Destroy(leaderboardUiList[i].gameObject);
            }
            
            leaderboardUiList.Clear();
            
            templateLeaderboardUi.gameObject.SetActive(false);
            
            blocker.gameObject.SetActive(false);
        }
        
        private void OnDataRequestResponse(LeaderboardDataResponse obj)
        {
            requestDataButton.interactable = true;
            blocker.gameObject.SetActive(false);
            
            if(obj.HasErrors)
                return;

            ResetLeaderboardUI();
            
            templateLeaderboardUi.gameObject.SetActive(true);
            
            var dataList = obj.Data.ToList();
            foreach (var data in dataList)
            {
                LeaderboardUI instance = Instantiate(templateLeaderboardUi.gameObject).GetComponent<LeaderboardUI>();
                
                instance.transform.SetParent(templateLeaderboardUi.transform.parent, false);
                
                instance.Setup((int)data.Rank, data.UserName, (int)data.GetNumberValue("SC_LeaderboardScore"));
                leaderboardUiList.Add(instance);
            }
            
            templateLeaderboardUi.gameObject.SetActive(false);
        }
        
        public void RequestData(Action<LeaderboardDataResponse> callback)
        {
            
            
            new GameSparks.Api.Requests.LeaderboardDataRequest()
                .SetLeaderboardShortCode("SC_Leaderboard")
                .SetEntryCount(10)
                .Send(callback);
        }
    }
}