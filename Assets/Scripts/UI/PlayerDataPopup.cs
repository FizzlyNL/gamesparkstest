﻿using System;
using GameSparks.Api.Responses;
using GameSparks.Core;
using JP.GameSparksTest.Data;
using UnityEngine;
using UnityEngine.UI;

namespace JP.GameSparksTest.UI
{
    public class PlayerDataPopup : BaseConnectedPopup
    {
        private PlayerData playerData = new PlayerData();

        [SerializeField] private Button addScoreButton;
        [SerializeField] private Button addGemsButton;
        [SerializeField] private Button addCoinsButton;

        [SerializeField] private Text scoreText;
        [SerializeField] private Text gemsText;
        [SerializeField] private Text coinsText;
        
        [SerializeField] private Button saveButton;

        protected override void Show()
        {
            base.Show();
            
            addScoreButton.onClick.AddListener(OnScoreButton);
            addGemsButton.onClick.AddListener(OnGemsButton);
            addCoinsButton.onClick.AddListener(OnCoinsButton);
            
            saveButton.onClick.AddListener(OnSaveButton);

            UpdateView();
        }

        protected override void Hide()
        {
            base.Hide();
            
            addScoreButton.onClick.RemoveAllListeners();
            addGemsButton.onClick.RemoveAllListeners();
            addCoinsButton.onClick.RemoveAllListeners();
            
            saveButton.onClick.RemoveAllListeners();
        }

        private void UpdateView()
        {
            if (playerData == null)
            {
                scoreText.text = "";
                gemsText.text = "";
                coinsText.text = ""; 
                return;
            }

            scoreText.text = playerData.Score.ToString();
            gemsText.text = playerData.Gems.ToString();
            coinsText.text = playerData.Coins.ToString();
        }

        private void OnSaveButton()
        {
            string playerDataJson = playerData.ToJson();

            SendLeaderboardData(null);
            
            SendPlayerData(playerDataJson, OnPlayerDataResponse);
        }

        private void OnPlayerDataResponse(CustomResponse obj)
        {
            if (obj.HasErrors)
                MessagePopup.Instance.ShowMessage("Error!", "Problem receiving player data! ");
            else
            {
                string playerDataJson = obj.ScriptData.GetGSData("SC_PlayerData").JSON;
                playerData = JsonUtility.FromJson<PlayerData>(playerDataJson);

                UpdateView();                                
            }
        }

        private void OnCoinsButton()
        {
            playerData.Coins++;
            UpdateView();  
        }

        private void OnGemsButton()
        {
            playerData.Gems++;
            UpdateView();  
        }

        private void OnScoreButton()
        {
            playerData.Score++;
            UpdateView();  
        }

        protected override void OnAuthentication(AuthenticationResponse obj)
        {
            base.OnAuthentication(obj);

            if (obj.HasErrors)
                return;


            string playerDataJson = obj.ScriptData.GetGSData("SC_PlayerData").JSON;
            playerData = JsonUtility.FromJson<PlayerData>(playerDataJson);


            UpdateView();
        }
        
        public void SendLeaderboardData(Action<CustomResponse> callback)
        {
            new GameSparks.Api.Requests.CustomRequest("LogEventRequest")
                .AddString("eventKey", "SC_LeaderboardScore")
                .AddNumber("SC_LeaderboardScore", playerData.Score)
                .Send(callback);
        }

        public void SendPlayerData(string data, Action<CustomResponse> callback)
        {
            GSRequestData requestData = new GSRequestData(data);
            
            new GameSparks.Api.Requests.CustomRequest("LogEventRequest")
                .AddString("eventKey", "SC_PlayerData")
                .AddObject("SC_PlayerData", requestData)
                .Send(callback);
        }
    }
}