﻿using System;
using UnityEngine;

namespace JP.GameSparksTest.Data
{
    [Serializable]
    public class PlayerData
    {
        public int Score;
        public int Gems;
        public int Coins;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
}